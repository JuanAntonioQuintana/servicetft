<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::resource('users', 'UsersController');
Route::resource('records', 'RecordsController');

Route::get('/records/index/{idAndroid}', 'RecordController@index');

Route::get('/records/index/{idAndroid}/{start_date?}/{end_date?}/{subject?}', 'RecordController@calculate');

Route::get('/records/filter/{idAndroid}/{subject}/{start_date?}/{end_date?}', 'RecordController@findUsers');

Route::get('/records/filterRecords/{idAndroid}/{subject}', 'RecordController@getRecords');