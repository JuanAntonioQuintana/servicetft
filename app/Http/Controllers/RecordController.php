<?php

namespace App\Http\Controllers;

use App\Record;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class RecordController extends Controller
{

    public function index($idAndroid){
        $record = DB::table('records')
            ->where([
                ['idAndroid', '=', $idAndroid],])
            ->get();


        return response()->json([
            'record' => $record
        ]);
    }

    public function findUsers($idAndroid, $subject, $start_date, $end_date){

        $start = new Carbon($start_date);
        $end = new Carbon($end_date);

        $recordsDate = DB::table('records')
            ->where([
                ['idAndroid', '=', $idAndroid], ['subject', '=', $subject]])->select('date')->distinct()
            ->get();

        $store_records_date = array();
        foreach($recordsDate as $record){
            if((new Carbon($record->date))->between($start, $end)){
                $store_records_date[] = $record;
            }

        }

        $records = DB::table('records')
            ->where([['subject', '=', $subject]])->select('idAndroid','subject','date')->distinct()
            ->get();


        $collection = collect([]);


        foreach($store_records_date as $element){

            foreach($records as $record){
               if(((new Carbon($element->date))->eq((new Carbon($record->date)))) && $record->idAndroid != $idAndroid){

                   if(!$collection->contains($record->idAndroid)){
                       $collection->push($record->idAndroid);
                   }

               }
            }
        }

        $store_records = array();

        foreach($collection as $element){
            $store_records[] = $this->addUser($element);
        }


        return response()->json([
            'record' => $store_records
        ]);


    }

    public function addUser($idAndroid){

        $elements = array();

        $element = new \stdClass();

        $users = DB::table('users')
            ->get();


        foreach($users as $user){
            if($user->idAndroid == $idAndroid){
                $elements[] = $user;
            }
        }

        foreach($elements as $user){
            $element->idAndroid= $user->idAndroid;
            $element->name= $user->name;
            $element->dni= $user->dni;
            $element->email=$user->email;
            $element->status=$user->status;
            return $element;
        }

    }

    public function getRecords($idAndroid, $subject){

        $recordsDate = DB::table('records')
            ->where([
                ['idAndroid', '=', $idAndroid], ])->select('date')->distinct()
            ->get();

        $store_records = array();

        foreach($recordsDate as $dates){
            if($this->addElement($idAndroid, $subject, $dates) != null){
                $store_records[] = $this->addElement($idAndroid, $subject, $dates);
            }
        }


        return response()->json([
            'record' => $store_records
        ]);
    }

    public function calculate($idAndroid, $start_date=null, $end_date=null, $subject=null ){

        $records = DB::table('records')
            ->select('date')->distinct()
            ->get();


        $start = new Carbon($start_date);
        $end = new Carbon($end_date);

        $store_records = array();
        foreach($records as $record){
            if((new Carbon($record->date))->between($start, $end)){
                $store_records[] = $record;
            }

        }

        $store_elements = array();

        foreach($store_records as $dates){
            if($this->addElement($idAndroid, $subject, $dates) != null){
                $store_elements[] = $this->addElement($idAndroid, $subject, $dates);
            }
        }


        return response()->json([
            'record' => $store_elements
        ]);

    }

    function addElement($idAndroid, $subject, $dates){

        $elements = array();

        $element = new \stdClass();

        $records = DB::table('records')
            ->where([
                ['idAndroid', '=', $idAndroid], ['subject', '=', $subject]])
            ->get();

        foreach($records as $record){
            foreach($dates as $key => $date){
                if($date == $record->date){
                    $elements[] = $record;
                }
            }
        }

        $recordTimeInput = $this->calculateTimeEvent($elements, "input");
        $recordTimeOutput = $this->calculateTimeEvent($elements, "output");

        $total = $this->secondsToTime($recordTimeOutput-$recordTimeInput);

        foreach($elements as $record){
            $element->subject= $record->subject;
            $element->time= $total;
            $element->classroom= $record->classroom;
            $element->date=$record->date;
            $element->start_date_time=$record->start_date_time;
            $element->end_date_time=$record->end_date_time;
            return $element;
        }
    }

    function calculateTimeEvent($records, $event){
        $value = 0;
        foreach($records as $record){
            if($record->event == $event ){
                $value = $value + $record->time;
            }
        }
        return $value;

    }

    function secondsToTime($time){
        $milliseconds = $time % 1000;
        $time = ($time - $milliseconds) / 1000;
        $seconds = $time % 60;
        $time = ($time - $seconds) / 60;
        $minutes = $time % 60;
        $hour = ($time - $minutes) / 60;

        if($hour < 10){
            $hour = '0'.$hour;
        }else{
            $hour = ''.$hour;
        }
        if($minutes < 10){
            $minutes ='0'.$minutes;
        }else{
            $minutes = ''.$minutes;
        }
        if($seconds < 10){
            $seconds = '0'.$seconds;
        }else{
            $seconds = ''.$seconds;
        }

        return $hour . ":" .$minutes . ":". $seconds;
    }

}
