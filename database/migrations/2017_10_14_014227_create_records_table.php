<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('records', function (Blueprint $table) {
            $table->increments('id');
            $table->string('idAndroid')->unsigned()->nullable();
            $table->foreign('idAndroid')->references('idAndroid')->on('users');
            $table->string('subject');
            $table->string('time');
            $table->string('classroom');
            $table->string('date');
            $table->string('start_date_time');
            $table->string('end_date_time');
            $table->string('event');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('records');
    }
}
